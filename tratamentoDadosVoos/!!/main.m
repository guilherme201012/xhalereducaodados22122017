%--------------------------------------------------------------------------
%
% POST PROCESSOR FOR ITA X-HALE FLIGHT TESTS
% Version 0
% Date: 2017-11-30
% Author(s): Ant�nio Bernardo Guimar�es Neto
%
%--------------------------------------------------------------------------

clear all
close all
fclose('all');
clc

global all_data

monitored_variables

%--------------------------------------------------------------------------
% Default data file names:

files_folder = '.\';

left_pod_filename = 'pod_esquerdo_voo1.FDR';
central_pod_filename = 'pod_central_voo1.FDR';
right_pod_filename = 'pod_direito_voo1.FDR';
arduino_filename = 'L23.TXT';

%--------------------------------------------------------------------------
% Generates all_data variable:
    
all_data = struct('left_pod_filename',left_pod_filename,...
    'central_pod_filename',central_pod_filename,...
    'right_pod_filename',right_pod_filename,...
    'arduino_filename',arduino_filename,...
    'left_pod_header',{left_pod_table},...
    'central_pod_header',{central_pod_table},...
    'right_pod_header',{right_pod_table},...
    'arduino_header',{arduino_table},...
    'left_pod_raw_data',0,...
    'central_pod_raw_data',0,...
    'right_pod_raw_data',0,...
    'arduino_raw_data',0,...
    'left_pod_pp_data',0,...
    'central_pod_pp_data',0,...
    'right_pod_pp_data',0,...
    'arduino_pp_data',0,...
    't0_left_pod_ms',-490400,...
    't0_central_pod_ms',-489400,...
    't0_right_pod_ms',-496500,...
    't0_arduino_ms',-562520,...
    'k_time_l_eagletree_GUI',1.0,...
    'k_time_c_eagletree_GUI',1.0,...
    'k_time_r_eagletree_GUI',1.0,...
    'k_time_l_eagletree',8.881610000000001e+02/8.833000000000001e+02,...
    'k_time_c_eagletree',8.881610000000001e+02/8.833500000000000e+02,...
    'k_time_r_eagletree',8.881610000000001e+02/8.831500000000001e+02,...
    'k_time_arduino',1.0,...
    'd_l_ail_raw_limits',[109 149],...
    'd_l_ail_deg_limits',[18.5 -11],...
    'd_r_ail_raw_limits',[158 120],...
    'd_r_ail_deg_limits',[-20.5 8.5],...
    'd_elev_raw_limits',[87 134],...
    'd_elev_deg_limits',[-12 15],...
    'd_r_ail_raw_limits_arduino',[1676 1452],...
    'd_r_ail_deg_limits_arduino',[-20.5 8.5],...
    'throttle_raw_limits',[61 148],...
    'throttle_limits',[0 1],...
    'flip_raw_limits',[28 229],...
    'flip_limits',[0 1],...
    'all_table',{all_table},...
    'all_variables',{all_variables});

%--------------------------------------------------------------------------
% Loads data:

% Left pod:
left_pod_filepath = strcat(files_folder,left_pod_filename);
left_pod_raw_data = read_POD(left_pod_filepath,left_pod_table);
all_data.left_pod_raw_data = left_pod_raw_data;
left_pod_pp_data = read_POD(left_pod_filepath,left_pod_table,left_pod_raw_data);
all_data.left_pod_pp_data = left_pod_pp_data;
for i_data=1:size(left_pod_table,1)
    if strfind(left_pod_table{i_data,3},'elev')
        all_data.left_pod_pp_data(:,i_data) = cs_raw2deg(all_data.left_pod_pp_data(:,i_data),all_data.d_elev_raw_limits,all_data.d_elev_deg_limits);
    elseif strfind(left_pod_table{i_data,3},'l_ail')
        all_data.left_pod_pp_data(:,i_data) = cs_raw2deg(all_data.left_pod_pp_data(:,i_data),all_data.d_l_ail_raw_limits,all_data.d_l_ail_deg_limits);
    elseif strfind(left_pod_table{i_data,3},'r_ail')
        all_data.left_pod_pp_data(:,i_data) = cs_raw2deg(all_data.left_pod_pp_data(:,i_data),all_data.d_r_ail_raw_limits,all_data.d_r_ail_deg_limits);
    elseif strfind(left_pod_table{i_data,3},'throttle')
        all_data.left_pod_pp_data(:,i_data) = engine_raw2frac(all_data.left_pod_pp_data(:,i_data),all_data.throttle_raw_limits,all_data.throttle_limits);
    elseif strfind(left_pod_table{i_data,3},'flip')
        all_data.left_pod_pp_data(:,i_data) = engine_raw2frac(all_data.left_pod_pp_data(:,i_data),all_data.flip_raw_limits,all_data.flip_limits);
    elseif strfind(left_pod_table{i_data,3},'pitot')
        all_data.left_pod_pp_data(:,i_data) = all_data.left_pod_pp_data(:,i_data)/3.6;
    elseif strfind(left_pod_table{i_data,3},'Speed')
        all_data.left_pod_pp_data(:,i_data) = all_data.left_pod_pp_data(:,i_data)/3.6;
    elseif strfind(left_pod_table{i_data,3},'RPM')
        all_data.left_pod_pp_data(:,i_data) = all_data.left_pod_pp_data(:,i_data)/4;
    end
end

% Central pod:
central_pod_filepath = strcat(files_folder,central_pod_filename);
central_pod_raw_data = read_POD(central_pod_filepath,central_pod_table);
all_data.central_pod_raw_data = central_pod_raw_data;
central_pod_pp_data = read_POD(central_pod_filepath,central_pod_table,central_pod_raw_data);
all_data.central_pod_pp_data = central_pod_pp_data;
for i_data=1:size(central_pod_table,1)
    if strfind(central_pod_table{i_data,3},'l_ail')
        all_data.central_pod_pp_data(:,i_data) = cs_raw2deg(all_data.central_pod_pp_data(:,i_data),all_data.d_l_ail_raw_limits,all_data.d_l_ail_deg_limits);
    elseif strfind(central_pod_table{i_data,3},'r_ail')
        all_data.central_pod_pp_data(:,i_data) = cs_raw2deg(all_data.central_pod_pp_data(:,i_data),all_data.d_r_ail_raw_limits,all_data.d_r_ail_deg_limits);
    elseif strfind(central_pod_table{i_data,3},'elev')
        all_data.central_pod_pp_data(:,i_data) = cs_raw2deg(all_data.central_pod_pp_data(:,i_data),all_data.d_elev_raw_limits,all_data.d_elev_deg_limits);
    elseif strfind(central_pod_table{i_data,3},'throttle')
        all_data.central_pod_pp_data(:,i_data) = engine_raw2frac(all_data.central_pod_pp_data(:,i_data),all_data.throttle_raw_limits,all_data.throttle_limits);
    elseif strfind(central_pod_table{i_data,3},'flip')
        all_data.central_pod_pp_data(:,i_data) = engine_raw2frac(all_data.central_pod_pp_data(:,i_data),all_data.flip_raw_limits,all_data.flip_limits);
    elseif strfind(central_pod_table{i_data,3},'pitot')
        all_data.central_pod_pp_data(:,i_data) = all_data.central_pod_pp_data(:,i_data)/3.6;
    elseif strfind(central_pod_table{i_data,3},'Speed')
        all_data.central_pod_pp_data(:,i_data) = all_data.central_pod_pp_data(:,i_data)/3.6;
    elseif strfind(central_pod_table{i_data,3},'RPM')
        all_data.central_pod_pp_data(:,i_data) = all_data.central_pod_pp_data(:,i_data)/4;
    end
end

% Right pod:
right_pod_filepath = strcat(files_folder,right_pod_filename);
right_pod_raw_data = read_POD(right_pod_filepath,right_pod_table);
all_data.right_pod_raw_data = right_pod_raw_data;
right_pod_pp_data = read_POD(right_pod_filepath,right_pod_table,right_pod_raw_data);
all_data.right_pod_pp_data = right_pod_pp_data;
for i_data=1:size(right_pod_table,1)
    if strfind(right_pod_table{i_data,3},'l_ail')
        all_data.right_pod_pp_data(:,i_data) = cs_raw2deg(all_data.right_pod_pp_data(:,i_data),all_data.d_l_ail_raw_limits,all_data.d_l_ail_deg_limits);
    elseif strfind(right_pod_table{i_data,3},'r_ail')
        all_data.right_pod_pp_data(:,i_data) = cs_raw2deg(all_data.right_pod_pp_data(:,i_data),all_data.d_r_ail_raw_limits,all_data.d_r_ail_deg_limits);
    elseif strfind(right_pod_table{i_data,3},'elev')
        all_data.right_pod_pp_data(:,i_data) = cs_raw2deg(all_data.right_pod_pp_data(:,i_data),all_data.d_elev_raw_limits,all_data.d_elev_deg_limits);
    elseif strfind(right_pod_table{i_data,3},'throttle')
        all_data.right_pod_pp_data(:,i_data) = engine_raw2frac(all_data.right_pod_pp_data(:,i_data),all_data.throttle_raw_limits,all_data.throttle_limits);
    elseif strfind(right_pod_table{i_data,3},'flip')
        all_data.right_pod_pp_data(:,i_data) = engine_raw2frac(all_data.right_pod_pp_data(:,i_data),all_data.flip_raw_limits,all_data.flip_limits);
    elseif strfind(right_pod_table{i_data,3},'pitot')
        all_data.right_pod_pp_data(:,i_data) = all_data.right_pod_pp_data(:,i_data)/3.6;
    elseif strfind(right_pod_table{i_data,3},'Speed')
        all_data.right_pod_pp_data(:,i_data) = all_data.right_pod_pp_data(:,i_data)/3.6;
    elseif strfind(right_pod_table{i_data,3},'RPM')
        all_data.right_pod_pp_data(:,i_data) = all_data.right_pod_pp_data(:,i_data)/4;
    end
end

% X-HALE, Arduino:
arduino_filepath = strcat(files_folder,arduino_filename);
arduino_raw_data = read_ARDUINO(arduino_filepath,arduino_table);
all_data.arduino_raw_data = arduino_raw_data;
arduino_pp_data = read_ARDUINO(arduino_filepath,arduino_table,arduino_raw_data);
all_data.arduino_pp_data = arduino_pp_data;
for i_data=1:size(arduino_table,1)
    if not(isempty(strfind(arduino_table{i_data,3},'gps_')))
        all_data.arduino_pp_data(:,i_data) = all_data.arduino_pp_data(:,i_data)./(-100);
    end
    if strfind(arduino_table{i_data,3},'r_ail')
        all_data.arduino_pp_data(:,i_data) = cs_raw2deg(all_data.arduino_pp_data(:,i_data),all_data.d_r_ail_raw_limits_arduino,all_data.d_r_ail_deg_limits_arduino);
    elseif strfind(arduino_table{i_data,3},'q_deg_s')
        all_data.arduino_pp_data(:,i_data) = -all_data.arduino_pp_data(:,i_data);
    elseif strfind(arduino_table{i_data,3},'p_deg_s')
        all_data.arduino_pp_data(:,i_data) = -all_data.arduino_pp_data(:,i_data);
    elseif strfind(arduino_table{i_data,3},'r_deg_s')
        all_data.arduino_pp_data(:,i_data) = -all_data.arduino_pp_data(:,i_data);
    elseif strfind(arduino_table{i_data,3},'nx_lwt')
        all_data.arduino_pp_data(:,i_data) = -all_data.arduino_pp_data(:,i_data);
    elseif strfind(arduino_table{i_data,3},'ny_rwt')
        all_data.arduino_pp_data(:,i_data) = -all_data.arduino_pp_data(:,i_data);
    end
end
