function mainAnalise
clc;
clear all;
resposta = 's';
while resposta == 's' || resposta =='S'
%     analiseEntrada;
    uiload;
    %------------------------------------------
    % ETS - esquerdo
    [f_ailEsq,FFT_ailEsq] = geraDadosFFTSinal(exported_all_data.d_l_ail_deg);
    [f_elevEsq,FFT_elevEsq] = geraDadosFFTSinal(exported_all_data.d_l_elev_deg);
    [f_throttleEsq,FFT_throttleEsq] = geraDadosFFTSinal(exported_all_data.throttle_l);
    [f_RPMEsq,FFT_RPMEsq] = geraDadosFFTSinal(exported_all_data.RPM_l);
    [f_HEsq,FFT_HEsq] = geraDadosFFTSinal(exported_all_data.H_l);
    [f_VEsq,FFT_VEsq] = geraDadosFFTSinal(exported_all_data.V_l_pitot);
    [f_HGPSEsq,FFT_HGPSEsq] = geraDadosFFTSinal(exported_all_data.GPS_l_Alt);
    [f_CourseGPSEsq,FFT_CourseGPSEsq] = geraDadosFFTSinal(exported_all_data.GPS_l_Course);
    tamanhoEsquerdo = max(size(f_elevEsq));
    %------------------------------------------
    % ETS - central
    [f_throttleCen,FFT_throttleCen] = geraDadosFFTSinal(exported_all_data.throttle_c);
    [f_RPMCen,FFT_RPMCen] = geraDadosFFTSinal(exported_all_data.RPM_c);
    [f_HCen,FFT_HCen] = geraDadosFFTSinal(exported_all_data.H_c);
    [f_VCen,FFT_VCen] = geraDadosFFTSinal(exported_all_data.V_c_pitot);
    [f_HGPSCen,FFT_HGPSCen] = geraDadosFFTSinal(exported_all_data.GPS_c_Alt);
    [f_CourseGPSCen,FFT_CourseGPSCen] = geraDadosFFTSinal(exported_all_data.GPS_c_Course);
    tamanhoCentral = max(size(f_HCen));
    %------------------------------------------
    % ETS - direito
    [f_ailDir,FFT_ailDir] = geraDadosFFTSinal(exported_all_data.d_r_ail_deg);
    [f_elevDir,FFT_elevDir] = geraDadosFFTSinal(exported_all_data.d_r_elev_deg);
    [f_throttleDir,FFT_throttleDir] = geraDadosFFTSinal(exported_all_data.throttle_r);
    [f_RPMDir,FFT_RPMDir] = geraDadosFFTSinal(exported_all_data.RPM_r);
    [f_HDir,FFT_HDir] = geraDadosFFTSinal(exported_all_data.H_r);
    [f_VDir,FFT_VDir] = geraDadosFFTSinal(exported_all_data.V_r_pitot);
    [f_HGPSDir,FFT_HGPSDir] = geraDadosFFTSinal(exported_all_data.GPS_r_Alt);
    [f_CourseGPSDir,FFT_CourseGPSDir] = geraDadosFFTSinal(exported_all_data.GPS_r_Course);
    tamanhoDireito = max(size(f_elevDir));
    %------------------------------------------
    % Arduino
    [f_nxCentral,FFT_nxCentral] = geraDadosFFTSinal(exported_all_data.nx_central);
    [f_nyCentral,FFT_nyCentral] = geraDadosFFTSinal(exported_all_data.ny_central);
    [f_nzCentral,FFT_nzCentral] = geraDadosFFTSinal(exported_all_data.nz_central);
    [f_nxEsq,FFT_nxEsq] = geraDadosFFTSinal(exported_all_data.nx_lwt);
    [f_nyEsq,FFT_nyEsq] = geraDadosFFTSinal(exported_all_data.ny_lwt);
    [f_nzEsq,FFT_nzEsq] = geraDadosFFTSinal(exported_all_data.nz_lwt);
    [f_nxDir,FFT_nxDir] = geraDadosFFTSinal(exported_all_data.nx_rwt);
    [f_nyDir,FFT_nyDir] = geraDadosFFTSinal(exported_all_data.ny_rwt);
    [f_nzDir,FFT_nzDir] = geraDadosFFTSinal(exported_all_data.nz_rwt);
    [f_pCentral,FFT_pCentral] = geraDadosFFTSinal(exported_all_data.p_deg_s);
    [f_qCentral,FFT_qCentral] = geraDadosFFTSinal(exported_all_data.q_deg_s);
    [f_rCentral,FFT_rCentral] = geraDadosFFTSinal(exported_all_data.r_deg_s);
    tamanhoArduino = max(size(f_nyDir));
    %------------------------------------------
    % Obten��o das raz�es entre entrada e sa�das
    tamanho = min([ tamanhoEsquerdo tamanhoDireito tamanhoCentral tamanhoArduino]);
    freq = f_elevEsq(1:tamanho);
    % ETS esquerdo
    FFTRazao_HEsq = abs(FFT_HEsq(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_VEsq = abs(FFT_VEsq(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_HGPSEsq = abs(FFT_HGPSEsq(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_CourseGPSEsq = abs(FFT_CourseGPSEsq(1:tamanho)./FFT_elevEsq(1:tamanho));
    % ETS central
    FFTRazao_HCen = abs(FFT_HCen(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_VCen = abs(FFT_VCen(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_HGPSCen = abs(FFT_HGPSCen(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_CourseGPSCen = abs(FFT_CourseGPSCen(1:tamanho)./FFT_elevEsq(1:tamanho));
    % ETS direito
    FFTRazao_HDir = abs(FFT_HDir(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_VDir = abs(FFT_VDir(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_HGPSDir = abs(FFT_HGPSDir(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_CourseGPSDir = abs(FFT_CourseGPSDir(1:tamanho)./FFT_elevEsq(1:tamanho));    
    % Arduino
    FFTRazao_nxCentral = abs(FFT_nxCentral(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_nyCentral = abs(FFT_nyCentral(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_nzCentral = abs(FFT_nzCentral(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_nxEsq = abs(FFT_nxEsq(1:tamanho)./FFT_elevEsq(1:tamanho));       
    FFTRazao_nyEsq = abs(FFT_nyEsq(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_nzEsq = abs(FFT_nzEsq(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_nxDir = abs(FFT_nxDir(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_nyDir = abs(FFT_nyDir(1:tamanho)./FFT_elevEsq(1:tamanho));     
    FFTRazao_nzDir = abs(FFT_nzDir(1:tamanho)./FFT_elevEsq(1:tamanho));       
    FFTRazao_pCentral = abs(FFT_pCentral(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_qCentral = abs(FFT_qCentral(1:tamanho)./FFT_elevEsq(1:tamanho));
    FFTRazao_rCentral = abs(FFT_rCentral(1:tamanho)./FFT_elevEsq(1:tamanho));
    
    figure;
    subplot(5,3,1);plot(freq,FFTRazao_HEsq,'o-r',freq,FFTRazao_HCen,'o-k',freq,FFTRazao_HDir,'o-g',...
        freq,FFTRazao_HGPSEsq,'.r',freq,FFTRazao_HGPSCen,'.k', freq,FFTRazao_HGPSDir,'.g');
    title('Altitude')
    subplot(5,3,2);plot(freq,FFTRazao_VEsq,'o-r',freq,FFTRazao_VCen,'o-k',freq,FFTRazao_VDir,'o-g');
    title('Velocidade')
    subplot(5,3,3);plot(freq,FFTRazao_CourseGPSEsq,'o-r',freq,FFTRazao_CourseGPSCen,'o-k',freq,FFTRazao_CourseGPSDir,'o-g');
    title('Altitude GPS')
    
    subplot(5,3,4);plot(freq,FFTRazao_pCentral,'o-k');
    title('p - central')
    subplot(5,3,5);plot(freq,FFTRazao_qCentral,'o-k');
    title('q - central')
    subplot(5,3,6);plot(freq,FFTRazao_rCentral,'o-k');
    title('r - central')    
    
    subplot(5,3,7);plot(freq,FFTRazao_nxCentral,'o-k');
    title('n_x - central')
    subplot(5,3,8);plot(freq,FFTRazao_nyCentral,'o-k');
    title('n_y - central')
    subplot(5,3,9);plot(freq,FFTRazao_nzCentral,'o-k');
    title('n_z - central')
    
    subplot(5,3,10);plot(freq,FFTRazao_nxEsq,'o-r');
    title('n_x - BB')
    subplot(5,3,11);plot(freq,FFTRazao_nyEsq,'o-r');
    title('n_y - BB')
    subplot(5,3,12);plot(freq,FFTRazao_nzEsq,'o-r');
    title('n_z - BB')
    
    subplot(5,3,13);plot(freq,FFTRazao_nxDir,'o-g');
    title('n_x - BE')
    subplot(5,3,14);plot(freq,FFTRazao_nyDir,'o-g');
    title('n_y - BE')
    subplot(5,3,15);plot(freq,FFTRazao_nzDir,'o-g');
    title('n_z - BE')
    
    %------------------------------------------
    resposta = input('deseja obter mais um dado para realizar compara��o(s/n): ','s');
end
end

function [f,FFT] = geraDadosFFTSinal(sinal)
        deltaTempo     = sinal.t_s;
        parametro      = interp1(deltaTempo, sinal.values, [deltaTempo(1):0.035:deltaTempo(end)]);
        [FFT, f] = gerarFFT(parametro);
end