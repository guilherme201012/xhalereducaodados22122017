function [w EAileronEsq EAileronDir EElevatoEsq EElevatoDir ERudderEsq...
    ERudderDir ERudderMed ERPMEsq ERPMDir ERPMMedi ] = entradaEnergySpectrum
uiload;
% Spectrum de Energia
%% aileron esquerdo
deltaTempo   = exported_all_data.d_l_ail_deg.t_s;
deltaAileron = exported_all_data.d_l_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step 
[w,EAileronEsq] = EnergySpectrum(deltaAileron,dt1,0.01,20,100);     % energy spectrum
%% aileron direito
deltaTempo   = exported_all_data.d_r_ail_deg.t_s;
deltaAileron = exported_all_data.d_r_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step 
[w,EAileronDir] = EnergySpectrum(deltaAileron,dt1,0.01,20,100);     % energy spectrum
%% elevator Esquerdo
deltaTempo   = exported_all_data.d_l_elev_deg.t_s;
deltaElevato = exported_all_data.d_l_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,EElevatoEsq] = EnergySpectrum(deltaElevato,dt1,0.01,20,100);     % energy spectrum
%% elevator Direito
deltaTempo   = exported_all_data.d_r_elev_deg.t_s;
deltaElevato = exported_all_data.d_r_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,EElevatoDir] = EnergySpectrum(deltaElevato,dt1,0.01,20,100);     % energy spectrum
%% ruder esquerdo
deltaTempo   = exported_all_data.throttle_l.t_s;
deltaRudder  = exported_all_data.throttle_l.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERudderEsq] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
%% ruder direito
deltaTempo   = exported_all_data.throttle_r.t_s;
deltaRudder  = exported_all_data.throttle_r.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERudderDir] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
%% ruder mediano
deltaTempo   = exported_all_data.throttle_c.t_s;
deltaRudder  = exported_all_data.throttle_c.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERudderMed] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
%% rpm esquerdo
deltaTempo   = exported_all_data.RPM_l.t_s;
deltaRPM     = exported_all_data.RPM_l.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERPMEsq]  = EnergySpectrum(deltaRPM,dt1,0.01,20,100);     % energy spectrum
%% ruder direito
deltaTempo   = exported_all_data.RPM_r.t_s;
deltaRPM     = exported_all_data.RPM_r.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERPMDir] = EnergySpectrum(deltaRPM,dt1,0.01,20,100);     % energy spectrum
%% ruder mediano
deltaTempo   = exported_all_data.RPM_c.t_s;
deltaRudder  = exported_all_data.RPM_c.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERPMMedi] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
% %--------------------------------------------------------------------------

end