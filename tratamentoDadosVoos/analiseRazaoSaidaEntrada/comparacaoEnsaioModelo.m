function comparacaoEnsaioModelo(posicaoEntrada, nomeArquivo)
warning off;
%entradas de profundor
% posicaoEntrada = 2;
% nomeArquivo = 'double01CabPicarVoo01';
if posicaoEntrada ==2
% [magQ_prof magP_prof  magR_prof  magZP_prof  magV_prof woutQ_prof  woutP_prof...
%     woutR_prof  woutZP_prof  woutV_prof SYSN] = saidaModelo(posicaoEntrada);
        [magQ magP  magR  magZP  magV woutQ  woutP...
    woutR  woutZP  woutV SYSN] = saidaModelo(posicaoEntrada);
else if posicaoEntrada == 1
        [magQ magP  magR  magZP  magV woutQ  woutP...
    woutR  woutZP  woutV SYSN] = saidaModelo(posicaoEntrada);
    else if posicaoEntrada == 9
            [magQ magP  magR  magZP  magV woutQ  woutP...
    woutR  woutZP  woutV SYSN] = saidaModelo(posicaoEntrada);
        end
    end
end
[freq FFTRazao_HEsq FFTRazao_VEsq FFTRazao_HGPSEsq FFTRazao_CourseGPSEsq ...
    FFTRazao_HCen FFTRazao_VCen FFTRazao_HGPSCen FFTRazao_CourseGPSCen FFTRazao_HDir ...
    FFTRazao_VDir FFTRazao_HGPSDir FFTRazao_CourseGPSDir FFTRazao_nxCentral ...
    FFTRazao_nyCentral FFTRazao_nzCentral FFTRazao_nxEsq FFTRazao_nyEsq ...
    FFTRazao_nzEsq FFTRazao_nxDir FFTRazao_nyDir FFTRazao_nzDir FFTRazao_pCentral...
    FFTRazao_qCentral FFTRazao_rCentral FFT_input exported_all_data] = saidaEnsaioVoo(posicaoEntrada);
[w EAileronEsq EAileronDir EElevatoEsq EElevatoDir ERudderEsq...
    ERudderDir ERudderMed ERPMEsq ERPMDir ERPMMedi ] = entradaEnergySpectrum;

%obter FFT para simula��o de fator de carga
[nx, ny, nz, nxEsq, nyEsq, nzEsq, nxDir, nyDir, nzDir]=...
    simulacaoLinear(exported_all_data,SYSN);
figure;
if posicaoEntrada ==2
    superficie = EElevatoEsq*.5+EElevatoDir*.5;
else if posicaoEntrada ==1
        superficie = EAileronDir*.5+EAileronDir*.5;
    else 
        superficie = ERPMEsq*.5+ERPMDir*.5;
end
end
plotar(w,superficie,freq,FFTRazao_VCen,woutV,magV,FFTRazao_HCen,woutZP,magZP,...
    FFTRazao_pCentral,woutP,magP,FFTRazao_qCentral,woutQ,magQ,FFTRazao_rCentral,...
    woutR, magR,nx, ny, nz,FFT_input,nxEsq, nyEsq, nzEsq,nxDir, nyDir, nzDir,...
    FFTRazao_nxCentral,FFTRazao_nyCentral,FFTRazao_nzCentral,FFTRazao_nxEsq,...
    FFTRazao_nyEsq, FFTRazao_nzEsq,FFTRazao_nxDir,FFTRazao_nyDir, FFTRazao_nzDir,...
    nomeArquivo);
end
function [f,FFT] = geraDadosFFTSinal(sinal)
        deltaTempo     = sinal.t_s;
        parametro      = interp1(deltaTempo, sinal.values, [deltaTempo(1):0.035:deltaTempo(end)]);
        [FFT, f] = gerarFFT(parametro);
end
function [f, FFT_nx,FFTRazao_NX, FFT_ny, FFTRazao_NY, FFT_nz, FFTRazao_NZ]...
    = dominioFrequencia(nx, ny, nz,FFT_input)
 [f,FFT_nx] = geraDadosFFTSinal(nx);
 tamanho = min([max(size(FFT_nx)) max(size(FFT_input))]);
 FFTRazao_NX = abs(FFT_nx(1:tamanho)./FFT_input(1:tamanho));
 [f,FFT_ny] = geraDadosFFTSinal(ny);
 tamanho = min([max(size(FFT_ny)) max(size(FFT_input))]);
 FFTRazao_NY = abs(FFT_ny(1:tamanho)./FFT_input(1:tamanho));
 [f,FFT_nz] = geraDadosFFTSinal(nz);
 tamanho = min([max(size(FFT_nz)) max(size(FFT_input))]);
 FFTRazao_NZ = abs(FFT_nz(1:tamanho)./FFT_input(1:tamanho));
end
function [nx, ny, nz, nxEsq, nyEsq, nzEsq, nxDir, nyDir, nzDir]=...
    simulacaoLinear(exported_all_data,SYSN);
deltaTempoAil   = exported_all_data.d_l_ail_deg.t_s;
deltaTempoElev   = exported_all_data.d_l_elev_deg.t_s;
deltaTempoMin = max(deltaTempoAil(1), deltaTempoElev(1));
deltaTempoMax = min(deltaTempoAil(end), deltaTempoElev(end));
deltaAileron = exported_all_data.d_l_ail_deg.values;
deltaAileron = interp1(deltaTempoAil, deltaAileron, [deltaTempoMin:0.035:deltaTempoMax]);
deltaTempo   = exported_all_data.d_l_elev_deg.t_s;
deltaElevato = exported_all_data.d_l_elev_deg.values;
deltaElevato = interp1(deltaTempoElev, deltaElevato, [deltaTempoMin:0.035:deltaTempoMax]);
deltaTempo = [deltaTempoMin:0.035:deltaTempoMax];
entrada = [deltaAileron' deltaElevato' deltaElevato' -deltaAileron' zeros(max(size(deltaElevato)),7)];
y = lsim(SYSN,entrada, deltaTempo);
nx.values = y(:,75);ny.values = y(:,76);nz.values = y(:,77);
nx.t_s = deltaTempo; ny.t_s = nx.t_s; nz.t_s = nx.t_s;
nxEsq.values = y(:,45);nyEsq.values = y(:,46);nzEsq.values = y(:,47);
nxEsq.t_s = deltaTempo; nyEsq.t_s = nx.t_s; nzEsq.t_s = nx.t_s;
nxDir.values = y(:,60);nyDir.values = y(:,61);nzDir.values = y(:,62);
nxDir.t_s = deltaTempo; nyDir.t_s = nx.t_s; nzDir.t_s = nx.t_s;
end
function plotar(w,superficie,freq,FFTRazao_VCen,woutV,magV,FFTRazao_HCen,woutZP,magZP,...
    FFTRazao_pCentral,woutP,magP,FFTRazao_qCentral,woutQ,magQ,FFTRazao_rCentral,...
    woutR, magR,nx, ny, nz,FFT_input,nxEsq, nyEsq, nzEsq,nxDir, nyDir, nzDir,...
    FFTRazao_nxCentral,FFTRazao_nyCentral,FFTRazao_nzCentral,FFTRazao_nxEsq,...
    FFTRazao_nyEsq, FFTRazao_nzEsq,FFTRazao_nxDir,FFTRazao_nyDir, FFTRazao_nzDir,...
    nomeArquivo)

subplot(5,3,1);plot(w,superficie,'b');xlim([0 15]);
ylabel('energy Spectrum (entrada)');xlabel('frequency, (1/s)'); 
subplot(5,3,2);
plot(freq,FFTRazao_VCen,'*-k');hold on;
plot(woutV.*1/(2*pi),magV,'b','linewidth',2);
ylabel('FFT_{v}/FFT_{input} (velocidade)');xlabel('frequency, (1/s)'); xlim([0 15]);
subplot(5,3,3);
plot(freq,FFTRazao_HCen,'*-k');hold on;
plot(woutZP.*1/(2*pi),magZP,'b','linewidth',2);
ylabel('FFT_{ZP}/FFT_{input} (altitude)');xlabel('frequency, (1/s)'); xlim([0 15]);

subplot(5,3,4);
plot(freq,FFTRazao_pCentral,'*-k');hold on;
plot(woutP.*1/(2*pi),magP,'b','linewidth',2);
ylabel('FFT_{p}/FFT_{input} (p)');xlabel('frequency, (1/s)'); xlim([0 15]);
subplot(5,3,5);
plot(freq,FFTRazao_qCentral,'*-k');hold on;
plot(woutQ.*1/(2*pi),magQ,'b','linewidth',2);
ylabel('FFT_{q}/FFT_{input} (q)');xlabel('frequency, (1/s)');xlim([0 15]);
subplot(5,3,6);
plot(freq,FFTRazao_rCentral,'*-k');hold on;
plot(woutR.*1/(2*pi),magR,'b','linewidth',2);
ylabel('FFT_{r}/FFT_{input} (r)');xlabel('frequency, (1/s)');xlim([0 15]);

[f, FFT_nx,FFTRazao_NX, FFT_ny, FFTRazao_NY, FFT_nz, FFTRazao_NZ] =...
     dominioFrequencia(nx, ny, nz,FFT_input);
 nyEsq.values = nyEsq.values.*cos(10*pi/180)-nzEsq.values.*sin(10*pi/180);
 nzEsq.values = nzEsq.values.*cos(10*pi/180)+nyEsq.values.*sin(10*pi/180);
[f, FFT_nxEsq,FFTRazao_NXEsq, FFT_nyEsq, FFTRazao_NYEsq, FFT_nzEsq, FFTRazao_NZEsq] =...
     dominioFrequencia(nxEsq, nyEsq, nzEsq,FFT_input);
 nyDir.values = nyDir.values.*cos(10*pi/180)-nzDir.values.*sin(10*pi/180);
 nzDir.values = nzDir.values.*cos(10*pi/180)+nyDir.values.*sin(10*pi/180);
[f, FFT_nxDir,FFTRazao_NXDir, FFT_nyDir, FFTRazao_NYDir, FFT_nzDir, FFTRazao_NZDir] =...
     dominioFrequencia(nxDir, nyDir, nzDir,FFT_input);
subplot(5,3,7);
plot(freq,FFTRazao_nxCentral,'*-k');hold on;
plot(f,FFTRazao_NX,'b','linewidth',2);hold on;
ylabel('FFT_{n_X}/FFT_{input} (N_x)');xlabel('frequency, (1/s)');xlim([0 15]);

subplot(5,3,8);
plot(freq,FFTRazao_nyCentral,'*-k');hold on;
plot(f,FFTRazao_NY,'b','linewidth',2);hold on;
ylabel('FFT_{n_Y}/FFT_{input} (N_y)');xlabel('frequency, (1/s)');xlim([0 15]);
title('RESPOSTAS DO MODELO LINEAR A ENTRADA NO SISTEMA E RESULTADOS DE ENSAIO EM VOO',...
    'fontweight','bold','fontsize',18);

subplot(5,3,9);
plot(freq,FFTRazao_nzCentral,'*-k');hold on;
plot(f,FFTRazao_NZ,'b','linewidth',2);hold on;
ylabel('FFT_{n_Z}/FFT_{input} (N_z)');xlabel('frequency, (1/s)');xlim([0 15]);

subplot(5,3,10);
plot(freq,FFTRazao_nxEsq,'*-k');hold on;
plot(f,FFTRazao_NXEsq,'r','linewidth',2);hold on;
ylabel('FFT_{n_X}/FFT_{input} (N_x_{ESQUERDO})');xlabel('frequency, (1/s)');xlim([0 15]);

subplot(5,3,11);
plot(freq,FFTRazao_nyEsq,'*-k');hold on;
plot(f,FFTRazao_NYEsq,'r','linewidth',2);hold on;
ylabel('FFT_{n_Y}/FFT_{input} (N_y_{ESQUERDO})');xlabel('frequency, (1/s)');xlim([0 15]);


subplot(5,3,12);
plot(freq,FFTRazao_nzEsq,'*-k');hold on;
plot(f,FFTRazao_NZEsq,'r','linewidth',2);hold on;
ylabel('FFT_{n_Z}/FFT_{input} (N_z_{ESQUERDO})');xlabel('frequency, (1/s)');xlim([0 15]);

subplot(5,3,13);
plot(freq,FFTRazao_nxDir,'*-k');hold on;
plot(f,FFTRazao_NXDir,'g','linewidth',2);hold on;
ylabel('FFT_{n_X}/FFT_{input} (N_x_{DIREITO})');xlabel('frequency, (1/s)');xlim([0 15]);

subplot(5,3,14);
plot(freq,FFTRazao_nyDir,'*-k');hold on;
plot(f,FFTRazao_NYDir,'g','linewidth',2);hold on;
ylabel('FFT_{n_Y}/FFT_{input} (N_y_{DIREITO})');xlabel('frequency, (1/s)');xlim([0 15]);

subplot(5,3,15);
plot(freq,FFTRazao_nzDir,'*-k');hold on;
plot(f,FFTRazao_NZDir,'g','linewidth',2);hold on;
ylabel('FFT_{n_Z}/FFT_{input} (N_z_{DIREITO})');xlabel('frequency, (1/s)');xlim([0 15]);

vetor01 = [freq' FFTRazao_VCen' freq' FFTRazao_HCen' freq' FFTRazao_pCentral'...
    freq' FFTRazao_qCentral' freq' FFTRazao_rCentral' freq' FFTRazao_nxCentral' f' FFTRazao_NX'...
    freq' FFTRazao_nyCentral' f' FFTRazao_NY' freq' FFTRazao_nzCentral' f' FFTRazao_NZ' ...
    freq' FFTRazao_nxEsq' f' FFTRazao_NXEsq' freq' FFTRazao_nyEsq' f' FFTRazao_NYEsq' ...
    freq' FFTRazao_nzEsq' f' FFTRazao_NZEsq' freq' FFTRazao_nxDir' f' FFTRazao_NXDir' ...
    freq' FFTRazao_nyDir' f' FFTRazao_NYDir' freq' FFTRazao_nzDir' f' FFTRazao_NZDir'];
vetorSuperficie = [w' superficie'] ;
vetorAltitude = [woutZP.*1/(2*pi) magZP]; 
vetorArfagemRatio = [woutQ.*1/(2*pi) magQ];
vetorVelocidade = [woutV.*1/(2*pi) magV];
vetorRolamento = [ woutP.*1/(2*pi) magP];
vetorGuinada = [woutR.*1/(2*pi) magR ];
save([nomeArquivo '.mat'], 'vetor01', 'vetorSuperficie','vetorAltitude',...
    'vetorArfagemRatio','vetorVelocidade', 'vetorRolamento', 'vetorGuinada');
end