function [magQ magP  magR  magZP  magV woutQ  woutP  woutR  woutZP  woutV SYSN]...
    = saidaModelo(posicaoEntrada)
% ULTIMA MODIFICACAO: 30/03/2018 
% POR: GUILHERME SOARES E SILVA
% INPUT
% posicaoEntrada: control position at row of controls
% OUTPUT
% mag : Magnitude of the system response in absolute units,To convert the ...
% magnitude from absolute units to decibels, use:20*log10(mag)
% wout :values are in radians per TimeUnit, where TimeUnit is the value of...
% the TimeUnit property of sys.
% pegar bizu do Ravindra!
posicaoEntrada=2;
% Equa��o de Estados
uiload;
Atio = - inv(E)*A;
Btio = - inv(E)*B;
Ctio = - (F*inv(E))*A + C;
Dtio = - (F*inv(E))*B + D;


% M-Q contribution  
A = Atio;
B = Btio(:,3);
C=Ctio(79,:);
D = Dtio(79,posicaoEntrada);
SYSMQ = ss(A,B,C,D);

% M-p contribution
B=Btio(:,9);
C=Ctio(78,:);
D = Dtio(78,posicaoEntrada);
SYSMP = ss(Atio,B,C,D);

% M-r contribution 
B=Btio(:,10);
C=Ctio(80,:);
D = Dtio(80,posicaoEntrada);
SYSMR = ss(Atio,B,C,D);

% % M-Zp contribution  
B=Btio(:,5);
C=zeros(1,max(size(B)));
D = 0;
SYSMZP = ss(Atio,B,C,D);
% 
% % M-V contribution  
B=Btio(:,1);
C=Ctio(1,:);
D = Dtio(1,posicaoEntrada);
SYSMV = ss(Atio,B,C,D);
% 
% % M-NZ/X/Y contribution  
SYSN = ss(Atio,Btio,Ctio,Dtio);

% 
% % Bode magnitude plot
[magQ,phase,woutQ] = bode(SYSMQ,{.2*pi 15*7});
magQ = squeeze(magQ);woutQ = squeeze(woutQ);
[magP,phase,woutP] = bode(SYSMP,{.2*pi 15*7});
magP = squeeze(magP);woutP = squeeze(woutP);
[magR,phase,woutR] = bode(SYSMR,{.2*pi 15*7});
magR = squeeze(magR);woutR = squeeze(woutR);
[magZP,phase,woutZP] = bode(SYSMZP,{.2*pi 15*7});
magZP = squeeze(magZP);woutZP = squeeze(woutZP);
[magV,phase,woutV] = bode(SYSMV,{.2*pi 15*7});
magV = squeeze(magV);woutV = squeeze(woutV);
% magnitude in absolute units (to decibels, use:20*log10(mag))
% Phase of the system response in degrees
%radians per TimeUnit, where TimeUnit is the value of the TimeUnit property of sys.
% mag = [magQ magP  magR  magZP  magV];
% wout=[woutQ  woutP  woutR  woutZP  woutV];
% mag = squeeze(magQ);
% wout = squeeze(woutQ);
% plot(wout,mag,'-s');
% semilogx(wout,20*log10(mag),'-s');
end


% bodemag(SYSMQ,'b',SYSMP,'k',SYSMR, {1 50},opts); grid
% legend('\theta','q','\phi','p','r','\psi','Z_P','\delta_{elevator}')
% 
% %plotar diagrama de polos e zero das fun��es de transfer�ncia
% 
% [poloTheta,zeroTheta]=pzmap(SYSMAL);
% [poloQ,zeroQ]=pzmap(SYSMQ);
% [poloZp,zeroZp]=pzmap(SYSMZP);
% [poloPhi,zeroPhi]=pzmap(SYSMPHI);
% [poloP,zeroP]=pzmap(SYSMP);
% [poloPsi,zeroPsi]=pzmap(SYSMPSI);
% [poloR,zeroR]=pzmap(SYSMR);
% figure()
% plot(real(poloTheta),imag(poloTheta),'+b');hold on;
% plot(real(poloQ),imag(poloQ),'*b');hold on;
% plot(real(poloZp),imag(poloZp),'xb');hold on;
% plot(real(poloPhi),imag(poloPhi),'or');hold on;
% plot(real(poloP),imag(poloP),'xm');hold on;
% plot(real(poloPsi),imag(poloPsi),'xy');hold on;
% plot(real(poloR),imag(poloR),'xg');hold on;
% legend('\theta - CP','q - CP','Z_P - LP','\Phi - espiral','p - rolam.',...
%     '\Psi - espiral','r - DR')
% grid on;
% 
% 
