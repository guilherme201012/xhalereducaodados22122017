% Author: Ravindra V. Jategaonkar
% published by AIAA, Reston, VA 20191, USA
%
% Inputs:
%     V        Vector with amplitudes of step signals
%     dtSig    Length of one time step in seconds
%     t0       Apply input signal starting at t = t0
%     T        Time vector
%
% Outputs:
%     sig      Time history of the input signal
%     f        Frequencies [1/s]
%     PSDsig   Energy spectrum
function [Y, f] = gerarFFT(signal)
Fs = 1/0.035; % Sampling frequency  
Y = fft(signal);%Compute the Fourier transform of the signal.
comprimentoSinal = length(Y);
Y = Y(1:(comprimentoSinal/2));
%Compute the two-sided spectrum P2. Then compute the single-sided spectrum P1 based on P2 and the even-valued signal length L.
% Pyy =   Y.*conj(Y)/comprimentoSinal;
% Pyy = Pyy(1:comprimentoSinal/2+1);
% P2 = abs(Y/comprimentoSinal);
% P1 = P2(1:comprimentoSinal/2+1);
% P1(2:end-1) = 2*P1(2:end-1);  
f = Fs*(1:(comprimentoSinal/2))/comprimentoSinal;%Define the frequency domain f 
% Pyy = sqrt(Pyy);
end
