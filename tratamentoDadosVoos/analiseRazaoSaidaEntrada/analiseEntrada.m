function analiseEntrada
uiload;
% Spectrum de Energia
%% aileron esquerdo
deltaTempo   = exported_all_data.d_l_ail_deg.t_s;
deltaAileron = exported_all_data.d_l_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step 
[w,EAileronEsq] = EnergySpectrum(deltaAileron,dt1,0.01,20,100);     % energy spectrum
%% aileron direito
deltaTempo   = exported_all_data.d_r_ail_deg.t_s;
deltaAileron = exported_all_data.d_r_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step 
[w,EAileronDir] = EnergySpectrum(deltaAileron,dt1,0.01,20,100);     % energy spectrum
%% elevator Esquerdo
deltaTempo   = exported_all_data.d_l_elev_deg.t_s;
deltaElevato = exported_all_data.d_l_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,EElevatoEsq] = EnergySpectrum(deltaElevato,dt1,0.01,20,100);     % energy spectrum
%% elevator Direito
deltaTempo   = exported_all_data.d_r_elev_deg.t_s;
deltaElevato = exported_all_data.d_r_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,EElevatoDir] = EnergySpectrum(deltaElevato,dt1,0.01,20,100);     % energy spectrum
%% ruder esquerdo
deltaTempo   = exported_all_data.throttle_l.t_s;
deltaRudder  = exported_all_data.throttle_l.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERudderEsq] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
%% ruder direito
deltaTempo   = exported_all_data.throttle_r.t_s;
deltaRudder  = exported_all_data.throttle_r.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERudderDir] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
%% ruder mediano
deltaTempo   = exported_all_data.throttle_c.t_s;
deltaRudder  = exported_all_data.throttle_c.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERudderMed] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
%% rpm esquerdo
deltaTempo   = exported_all_data.RPM_l.t_s;
deltaRPM     = exported_all_data.RPM_l.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERPMEsq]  = EnergySpectrum(deltaRPM,dt1,0.01,20,100);     % energy spectrum
%% ruder direito
deltaTempo   = exported_all_data.RPM_r.t_s;
deltaRPM     = exported_all_data.RPM_r.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERPMDir] = EnergySpectrum(deltaRPM,dt1,0.01,20,100);     % energy spectrum
%% ruder mediano
deltaTempo   = exported_all_data.RPM_c.t_s;
deltaRudder  = exported_all_data.RPM_c.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[w,ERPMMedi] = EnergySpectrum(deltaRudder,dt1,0.01,20,100);     % energy spectrum
% %--------------------------------------------------------------------------
% PSD 
t0 = 1;                      % apply input signal starting at t = t0
T  = [0:.1:10];             % Time vector
freq = linspace(0.01,20,100); 
%% aileron esquerdo
deltaTempo   = exported_all_data.d_l_ail_deg.t_s;
deltaAileron = exported_all_data.d_l_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step 
[sig, f1, PSDAileronEsq] = InputSigPSD(deltaAileron, dt1, t0, T);
PSDAileronEsq = interp1(f1, PSDAileronEsq, freq);
f1 = freq;
%% aileron direito
deltaTempo   = exported_all_data.d_r_ail_deg.t_s;
deltaAileron = exported_all_data.d_r_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step 
[sig, f2, PSDAileronDir] = InputSigPSD(deltaAileron, dt1, t0, T);
PSDAileronDir = interp1(f2, PSDAileronDir, freq);
f2 = freq;
%% elevator Esquerdo
deltaTempo   = exported_all_data.d_l_elev_deg.t_s;
deltaElevato = exported_all_data.d_l_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f3, PSDElevEsq] = InputSigPSD(deltaElevato, dt1, t0, T);
PSDElevEsq = interp1(f3, PSDElevEsq, freq);
f3 = freq;
%% elevator Direito
deltaTempo   = exported_all_data.d_r_elev_deg.t_s;
deltaElevato = exported_all_data.d_r_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f4, PSDElevDir] = InputSigPSD(deltaElevato, dt1, t0, T);
PSDElevDir = interp1(f4, PSDElevDir, freq);
f4 = freq;
%% ruder esquerdo
deltaTempo   = exported_all_data.throttle_l.t_s;
deltaRudder  = exported_all_data.throttle_l.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f5, PSDRudderEsq] = InputSigPSD(deltaRudder, dt1, t0, T);
PSDRudderEsq = interp1(f5, PSDRudderEsq, freq);
f5 = freq;
%% ruder direito
deltaTempo   = exported_all_data.throttle_r.t_s;
deltaRudder  = exported_all_data.throttle_r.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f6, PSDRudderDir] = InputSigPSD(deltaRudder, dt1, t0, T);
PSDRudderDir = interp1(f6, PSDRudderDir, freq);
f6 = freq;
%% ruder mediano
deltaTempo   = exported_all_data.throttle_c.t_s;
deltaRudder  = exported_all_data.throttle_c.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f7, PSDRudderMed] = InputSigPSD(deltaRudder, dt1, t0, T);
PSDRudderMed = interp1(f7, PSDRudderMed, freq);
f7 = freq;
%% rpm esquerdo
deltaTempo   = exported_all_data.RPM_l.t_s;
deltaRPM     = exported_all_data.RPM_l.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f8, PSDRPMEsq] = InputSigPSD(deltaRudder, dt1, t0, T);
PSDRPMEsq = interp1(f8, PSDRPMEsq, freq);
f8 = freq;
%% ruder direito
deltaTempo   = exported_all_data.RPM_r.t_s;
deltaRPM     = exported_all_data.RPM_r.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f9, PSDRPMDir] = InputSigPSD(deltaRudder, dt1, t0, T);
PSDRPMDir = interp1(f9, PSDRPMDir, freq);
f9 = freq;
%% ruder mediano
deltaTempo   = exported_all_data.RPM_c.t_s;
deltaRudder  = exported_all_data.RPM_c.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
dt1 = 0.035;                                        % time step
[sig, f10, PSDRPMMed] = InputSigPSD(deltaRudder, dt1, t0, T);
PSDRPMMed = interp1(f10, PSDRPMMed, freq);
f10 = freq;
% %--------------------------------------------------------------------------
% FFT
%% aileron esquerdo
deltaTempo   = exported_all_data.d_l_ail_deg.t_s;
deltaAileron = exported_all_data.d_l_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTAileronEsq, f_FFT1] = gerarFFT(deltaAileron);
FFTAileronEsq = interp1(f_FFT1, FFTAileronEsq, freq);
f_FFT1 = freq;
%% aileron direito
deltaTempo   = exported_all_data.d_r_ail_deg.t_s;
deltaAileron = exported_all_data.d_r_ail_deg.values;
deltaAileron = interp1(deltaTempo, deltaAileron, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTAileronDir, f_FFT2] = gerarFFT(deltaAileron);
FFTAileronDir = interp1(f_FFT2, FFTAileronDir, freq);
f_FFT2 = freq;
 %% elevator Esquerdo
deltaTempo   = exported_all_data.d_l_elev_deg.t_s;
deltaElevato = exported_all_data.d_l_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTElevatEsq, f_FFT3] = gerarFFT(deltaElevato);
FFTElevatEsq = interp1(f_FFT3, FFTElevatEsq, freq);
f_FFT3 = freq;
 %% elevator Direito
deltaTempo   = exported_all_data.d_r_elev_deg.t_s;
deltaElevato = exported_all_data.d_r_elev_deg.values;
deltaElevato = interp1(deltaTempo, deltaElevato, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTElevatDir, f_FFT4] = gerarFFT(deltaElevato);
FFTElevatDir = interp1(f_FFT4, FFTElevatDir, freq);
f_FFT4 = freq;
%% ruder esquerdo
deltaTempo   = exported_all_data.throttle_l.t_s;
deltaRudder  = exported_all_data.throttle_l.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTRudderEsq, f_FFT5] = gerarFFT(deltaRudder);
FFTRudderEsq = interp1(f_FFT5, FFTRudderEsq, freq);
f_FFT5 = freq;
%% ruder direito
deltaTempo   = exported_all_data.throttle_r.t_s;
deltaRudder  = exported_all_data.throttle_r.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTRudderDir, f_FFT6] = gerarFFT(deltaRudder);
FFTRudderDir = interp1(f_FFT6, FFTRudderDir, freq);
f_FFT6 = freq;
%% ruder mediano
deltaTempo   = exported_all_data.throttle_c.t_s;
deltaRudder  = exported_all_data.throttle_c.values;
deltaRudder  = interp1(deltaTempo, deltaRudder, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTRudderMed, f_FFT7] = gerarFFT(deltaRudder);
FFTRudderMed = interp1(f_FFT7, FFTRudderMed, freq);
f_FFT7 = freq;
%% rpm esquerdo
deltaTempo   = exported_all_data.RPM_l.t_s;
deltaRPM     = exported_all_data.RPM_l.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTRPMEsq, f_FFT8] = gerarFFT(deltaRPM);
FFTRPMEsq = interp1(f_FFT8, FFTRPMEsq, freq);
f_FFT8 = freq;
%% rPM direito
deltaTempo   = exported_all_data.RPM_r.t_s;
deltaRPM     = exported_all_data.RPM_r.values;
deltaRPM     = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTRPMDir, f_FFT9] = gerarFFT(deltaRPM);
FFTRPMDir = interp1(f_FFT9, FFTRPMDir, freq);
f_FFT9 = freq;
%% ruder mediano
deltaTempo   = exported_all_data.RPM_c.t_s;
deltaRPM  = exported_all_data.RPM_c.values;
deltaRPM  = interp1(deltaTempo, deltaRPM, [deltaTempo(1):0.035:deltaTempo(end)]);
[FFTRPMMed, f_FFT10] = gerarFFT(deltaRPM);
FFTRPMMed = interp1(f_FFT10, FFTRPMMed, freq);
f_FFT10 = freq;
    %----------------------------------------
subplot(3,4,1);plot(exported_all_data.d_l_ail_deg.t_s,exported_all_data.d_l_ail_deg.values,...
    exported_all_data.d_r_ail_deg.t_s,exported_all_data.d_r_ail_deg.values);
ylabel('\delta_{ail}');
subplot(3,4,5);plot(exported_all_data.d_l_elev_deg.t_s,exported_all_data.d_l_elev_deg.values,...
    exported_all_data.d_r_elev_deg.t_s,exported_all_data.d_r_elev_deg.values);
ylabel('\delta_{elev}');
subplot(3,4,9);plot(exported_all_data.throttle_r.t_s,exported_all_data.throttle_r.values,...
    exported_all_data.throttle_c.t_s,exported_all_data.throttle_c.values,...
    exported_all_data.throttle_l.t_s,exported_all_data.throttle_l.values);
ylabel('\delta_{Throtle}');legend('r','c','l');


subplot(3,4,2);plot(w,EAileronEsq,'b', w,EAileronDir,'r');title('aileron')
legend('l','r');ylabel('energy Spectrum')
subplot(3,4,3);plot(w,EElevatoEsq,'b', w,EElevatoDir,'r');title('elevator')
legend('l','r')
subplot(3,4,4);plot(w,ERudderEsq,'b', w,ERudderDir,'r',w,ERudderMed,...
    'y',w,ERPMEsq,'m',w,ERPMDir,'k',w,ERPMMedi,'g');title('rudder')
legend('PWMl','PWMr','PWMc','RPMl','RPMr','RPMc');
xlabel('frequency, (rad/s)'); 


subplot(3,4,6);plot(f1, PSDAileronEsq,'b', f2, PSDAileronDir,'r');title('aileron')
legend('l','r');ylabel('PSD')
subplot(3,4,7);plot(f3, PSDElevEsq,'b', f4, PSDElevDir,'r');title('elevator')
legend('l','r')
subplot(3,4,8);plot(f5, PSDRudderEsq,'b', f6, PSDRudderDir,'r',f7, PSDRudderMed,...
    'y',f8, PSDRPMEsq,'m',f9, PSDRPMDir,'k',f10, PSDRPMMed,'g');title('rudder')

subplot(3,4,10);plot(f_FFT1, FFTAileronEsq,'b', f_FFT2,FFTAileronDir,'r');title('aileron')
legend('l','r');ylabel('FFT')
subplot(3,4,11);plot(f_FFT3, FFTElevatEsq,'b',f_FFT4, FFTElevatDir,'r');title('elevator')
legend('l','r')
subplot(3,4,12);plot(f_FFT5, FFTRudderEsq,'b', f_FFT6, FFTRudderDir,'r',f_FFT7, FFTRudderMed,...
    'y',f_FFT8, FFTRPMEsq,'m',f_FFT9, FFTRPMDir,'k',f_FFT10, FFTRPMMed,'g');title('rudder')
    
end